# README #

* Title: DMA Upload Tool
* Created by: BlueJack Consulting Inc. 
* Date: July 25, 2018
* Description: Repo of simple ashx upload tool.

### What is this repository for? ###

* This repo contains Visual Studio projet file + contents for a simple ashx file upload tool.
* The tool contains an html document with jQuery permitting user to upload a file to a server hosting ashx generic handler which then saves file to the server.
* As of July 25 there is a ~5mb file upload size limit

### How do I get set up? ###

* Requires Visual Studio 2017 + IIS Express (not sure if that is bundled with Visual Studio or not)

### Who do I talk to? ###

* Repo owner: Alexander Wojcik (awojcik@bluejack.ca / alexdouglaswojcik@gmail.com)
* Other community or team contact: Rachel O'Neil (roneil@bluejack.ca)